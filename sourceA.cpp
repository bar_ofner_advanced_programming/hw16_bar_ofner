#include "sqlite3.h"
#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

int main(){


	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	rc = sqlite3_exec(db, "CREATE TABLE PEOPLE(ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT);", NULL, NULL, &zErrMsg);

	if (rc){
		cout << zErrMsg << endl;
	}

	rc = sqlite3_exec(db, "INSERT INTO PEOPLE (NAME) VALUES ('aaa');", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	rc = sqlite3_exec(db, "INSERT INTO PEOPLE (NAME) VALUES ('bbb');", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	rc = sqlite3_exec(db, "INSERT INTO PEOPLE (NAME) VALUES ('bbb');", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	rc = sqlite3_exec(db, "INSERT INTO PEOPLE (NAME) VALUES ('ccc');", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	rc = sqlite3_exec(db, "UPDATE PEOPLE SET NAME='ddd' WHERE ID = last_insert_rowid();", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	sqlite3_close(db);
	system("PAUSE");
	return 0;
}