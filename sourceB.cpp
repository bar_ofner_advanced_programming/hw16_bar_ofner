#include "sqlite3.h"
#include <iostream>
#include <string>
using namespace std;

int callbackBuyer(void* notUsed, int argc, char** argv, char** azCol);
int callbackCar(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

//globals for the buy query
int balance;
int price;

//globals for the money transfer
int moneyTo;
int moneyFrom;

int main(){
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db);
	if (rc){
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}



	rc = sqlite3_exec(db, "INSERT INTO cars (id,Model,color,price,available) VALUES (1,'a','white',2000,1);", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	
	rc = sqlite3_exec(db, "INSERT INTO buyers (id,Name) VALUES (1,'a');", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}

	rc = sqlite3_exec(db, "INSERT INTO accounts (id,Buyer_id,balance) VALUES (1,1,2500);", NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	cout << carPurchase(1,1,db,zErrMsg) << endl;

	sqlite3_close(db);
	system("PAUSE");
	return 0;
}

//callback for the car buying proccess - buyer side
int callbackBuyer(void* notUsed, int argc, char** argv, char** azCol){
	balance = atoi(azCol[0]);
	return 0;
}

int callbackCar(void* notUsed, int argc, char** argv, char** azCol){
	price = atoi(azCol[0]);
	return 0;
}


/* checks if the buyer can buy a car.
if it's true then the func will remove 
the money from the buyer's account and
will mark the car unavailable
else will return false
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg){
	int rc;
	//get buyer balance by query
	string q = string("select balance from accounts where Buyer_id=") + std::to_string(buyerid) + ";";
	rc = sqlite3_exec(db, q.c_str(), callbackBuyer, 0, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	//get car price by query
	q = string("select price from cars where id=") + std::to_string(carid) + ";";
	rc = sqlite3_exec(db, q.c_str(), callbackCar, 0, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	if (price <= balance){
		q = "UPDATE accounts SET balance=" + std::to_string(balance - price) + " WHERE buyer_id=" + std::to_string(buyerid) + ";";
		rc = sqlite3_exec(db, q.c_str(), NULL, NULL, &zErrMsg);
		if (rc){
			cout << zErrMsg << endl;
		}
		q = "UPDATE cars SET available=0 WHERE buyer_id=" + std::to_string(carid) + ";";
		rc = sqlite3_exec(db, q.c_str(), NULL, NULL, &zErrMsg);
		if (rc){
			cout << zErrMsg << endl;
		}
		return true;
	}
	return false;
}


int callbackFrom(void* notUsed, int argc, char** argv, char** azCol){
	moneyFrom = atoi(azCol[0]);
	return 0;
}

int callbackTo(void* notUsed, int argc, char** argv, char** azCol){
	moneyTo = atoi(azCol[0]);
	return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg){
	int rc;
	//get from balance by query
	string q = string("select balance from accounts where Buyer_id=") + std::to_string(from) + ";";
	rc = sqlite3_exec(db, q.c_str(), callbackFrom, 0, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	if (moneyFrom < amount){
		return false;
	}
	//get to balance by query
	q = string("select balance from accounts where Buyer_id=") + std::to_string(to) + ";";
	rc = sqlite3_exec(db, q.c_str(), callbackTo, 0, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	q = "UPDATE accounts SET balance=" + std::to_string(moneyFrom-amount) + " WHERE buyer_id=" + std::to_string(from) + ";";
	rc = sqlite3_exec(db, q.c_str(), NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}
	q = "UPDATE accounts SET balance=" + std::to_string(moneyTo + amount) + " WHERE buyer_id=" + std::to_string(to) + ";";
	rc = sqlite3_exec(db, q.c_str(), NULL, NULL, &zErrMsg);
	if (rc){
		cout << zErrMsg << endl;
	}

	return true;
}